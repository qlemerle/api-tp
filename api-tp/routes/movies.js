var express = require('express');
var router = express.Router();
var _ = require('lodash');

let movies = [{
  movie: "Star Wars",
  yearOfRelease: "1996",
  id: "0"
}];


/* GET users listing. */
router.get('/', (req, res) => {
  // Get List of user and return JSON
  res.status(200).json({ movies });
});


/* GET one user. */
router.get('/:id', (req, res) => {
  const { id } = req.params;
  // Find user in DB
  const movie = _.find(movies, ["id", id]);
  // Return user
  res.status(200).json({
    message: 'Movie found!',
    movie 
  });
});


/* PUT new user. */
router.put('/', (req, res) => {
  // Get the data from request from request
  const { movie, yearOfRelease } = req.body;
  // Create new unique id
  const id = _.uniqueId();
  // Insert it in array (normaly with connect the data with the database)
  movies.push({ movie, yearOfRelease, id });
  // Return message
  res.json({
    message: `Just added ${id}`,
    movie: { movie, yearOfRelease, id }
  });
});


/* DELETE user. */
router.delete('/:id', (req, res) => {
  // Get the :id of the user we want to delete from the params of the request
  const { id } = req.params;
  // Remove from "DB"
  _.remove(movies, ["id", id]);

  // Return message
  res.json({
    message: `Just removed ${id}`
  });
});

/* UPDATE user. */
router.post('/:id', (req, res) => {
  // Get the :id of the user we want to update from the params of the request
  const { id } = req.params;
  // Get the new data of the user we want to update from the body of the request
  const { movie, yearOfRelease } = req.body;
  // Find in DB
  const movieToUpdate = _.find(movies, ["id", id]);
  // Update data with new data (js is by address)
  movieToUpdate.movie = movie;
  movieToUpdate.yearOfRelease = yearOfRelease;

  // Return message
  res.json({
    message: `Just updated ${id} with ${movie} with ${yearOfRelease}`
  });
});



module.exports = router;